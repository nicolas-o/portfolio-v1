import resumeSpa from "../assets/NicolasOrellanaCV.pdf";
import resumeEng from "../assets/NicolasOrellanaCV(eng).pdf";

export const spanish = [
  {
    nav: ["Sobre Mi", "que Hago", "Proyectos", "Contactame"],
    header: [
      "Hola, mi nombre es",
      "Nicolas Orellana",
      "Front-end Developer",
      "Contactame",
    ],
    sobreMi: [
      "SOBRE MI",
      "Hola! Mi nombre es Nicolás Orellana, soy muy apasionado por las tecnologías web y actualmente estoy enfocado en el front-end. Estoy en tercer año estudiando un Bachelor of Science in Applied Technology ",
      " de Estados Unidos. No me quedo solo con los conocimientos recibidos en la universidad si no que mas bien busco detalladamente en otros canales de información asuntos de mi interés así que hace un año que estoy buscando adquirir por mi cuenta conocimientos más avanzados que me han llevado a mejorar mis habilidades.",
      "Aparte de eso me encanta cocinar cosas nuevas y esforzarme para que queden ricas. También me gusta disfrutar de las peliculas, analizarlas y encontrarles lo bueno.",
      "No quieres leer?...",
      "Películas",
      "Desarrollo Web",
      "Comida",
    ],
    queHago: [
      "QUE PUEDO HACER",
      "DESARROLLO",
      "Empecé programando en Python algunas aplicaciones para la universidad. Luego aprendí HTML, CSS y Javascript y me enamoré del desarrollo web. Actualmente me encuentro estudiando y desarrollando aplicaciones en React utilizando ES6 sintax. Mi aspiración es desarrollar como Full Stack.",
      "DISEÑO",
      "Puedo crear diseños no tan complejos en Illustrator, Photoshop e InDesign. Me esfuerzo mucho por crear estructuras con sentido y hacer que las aplicaciones se vean ordenadas, animadas y limpias utilizando algún preprocesador o librería como bootstrap, Material-UI, react-spring, etc.",
      "LIDERAZGO",
      "Durante dos años presté servicio voluntario como misionero en Japón y con el tiempo tuve la oportunidad de ser líder de grupo y eventualmente de grupos. Fue una experiencia enriquecedora donde pude aprender bastante, incluso hablar dos idiomas fluidamente y poder enseñarlos.",
      "Tecnologías: ",
    ],
    proyectos: ["PROYECTOS"],
    contactame: ["CONTACTAME", "Descarga mi CV", resumeSpa],
  },
];

export const english = [
  {
    nav: ["About", "What I Do", "Projects", "Contact Me"],
    header: [
      "Hi, my name is",
      "Nicolas Orellana",
      "Front-end Developer",
      "Contact Me",
    ],
    sobreMi: [
      "ABOUT ME",
      "Hello! My name is Nicolas Orellana. I'm very passionate about web technology and I'm currently focusing on Front-End Development. I'm majoring in BS in Applied Technology at ",
      ". I always try to seek more knowledge apart from what I'm learning at university, that's why I started to take internet courses on websites, such as LinkedIn, Freecodecamp, Youtube, etc. And improve my abilities even further.",
      "Apart from that, I love to cook tasty food and make an extra effort to make them delicious. I also love movies and find what's good about them.",
      "Don't want to read?...",
      "Movies",
      "Web Development",
      "Food",
    ],
    queHago: [
      "WHAT I DO",
      "DEVELOPMENT",
      "I started programming some applications in python for the university. Later, when I learned HTML, CSS, and Javascript, I fell in love with web development. Today I'm studying and developing applications with React using ES6 syntax. My goal is to become Full Stack Developer.",
      "DESIGN",
      "I can create simple designs in Illustrator, Photoshop, and InDesign. I do my best to create structures that make sense and applications that look organized, animated and clean using a preprocessor like Sass or libraries like Bootstrap, Material-UI, react-spring, etc.",
      "LIDERSHIP",
      "For two years I served as a volunteer missionary in Japan. I had the opportunity to be a group leader and eventually a leader of groups. It was an amazing experience where I learned so much, I even learned to speak two languages fluently and also been able to teach them.",
      "Technologies: ",
    ],
    proyectos: ["PROJECTS"],
    contactame: ["CONTACT ME", "Download Resume", resumeEng],
  },
];
